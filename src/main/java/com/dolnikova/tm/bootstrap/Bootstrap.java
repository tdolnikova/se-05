package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.command.exit.ExitCommand;
import com.dolnikova.tm.command.help.HelpCommand;
import com.dolnikova.tm.command.project.*;
import com.dolnikova.tm.command.task.*;
import com.dolnikova.tm.exception.CommandCorruptException;
import com.dolnikova.tm.repository.ProjectRepository;
import com.dolnikova.tm.repository.TaskRepository;
import com.dolnikova.tm.service.ProjectService;
import com.dolnikova.tm.service.TaskService;

import java.util.*;

public class Bootstrap {

    private final static Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    public static final Scanner scanner = new Scanner(System.in);
    public static ProjectRepository projectRepository = new ProjectRepository();
    public static ProjectService projectService = new ProjectService(projectRepository);
    public static TaskRepository taskRepository = new TaskRepository();
    public static TaskService taskService = new TaskService(taskRepository);

    public void init() {
        try {
            registry(new HelpCommand());
            registry(new ExitCommand());
            registry(new ProjectFindAllCommand());
            registry(new ProjectFindOneCommand());
            registry(new ProjectMergeCommand());
            registry(new ProjectPersistCommand());
            registry(new ProjectRemoveAllCommand());
            registry(new ProjectRemoveCommand());
            registry(new TaskFindAllCommand());
            registry(new TaskFindOneCommand());
            registry(new TaskMergeCommand());
            registry(new TaskPersistCommand());
            registry(new TaskRemoveAllCommand());
            registry(new TaskRemoveCommand());
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        commands.put(cliCommand, command);
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    public static List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

}
