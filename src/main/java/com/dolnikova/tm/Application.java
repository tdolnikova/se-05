package com.dolnikova.tm;

import com.dolnikova.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
