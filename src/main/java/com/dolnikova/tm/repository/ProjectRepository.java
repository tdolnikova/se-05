package com.dolnikova.tm.repository;

import com.dolnikova.tm.entity.Project;

import java.util.*;

public class ProjectRepository implements Repository<Project> {

    private Map<String, Project> projects = new LinkedHashMap<>();

    @Override
    public List<Project> findAll() {
        List<Project> allProjects = new ArrayList<>();
        for (Map.Entry<String, Project> project : projects.entrySet()) {
            allProjects.add(project.getValue());
        }
        return allProjects;
    }

    @Override
    public void removeAll() {
        projects.clear();
    }

    @Override
    public void persist(Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public Project findOne(String name) {
        for(Map.Entry<String, Project> project : projects.entrySet()) {
            Project foundProject = project.getValue();
            if (name.equals(foundProject.getName())) return foundProject;
        }
        return null;
    }

    @Override
    public void merge(String newProjectName, Project project) {
        project.setName(newProjectName);
    }

    @Override
    public void remove(String id) {
        projects.remove(id);
    }

    @Override
    public void remove(Project project) {
        projects.remove(project.getId());
    }

    public Map<String, Project> getProjects() {
        return projects;
    }
}
