package com.dolnikova.tm.repository;

import com.dolnikova.tm.entity.Entity;

import java.util.List;

public interface Repository <E extends Entity> {
    List<E> findAll();
    E findOne(String name);
    void persist(E entity);
    void merge(String newName, E entity);
    void remove(String id);
    void remove(E entity);
    void removeAll();
}
