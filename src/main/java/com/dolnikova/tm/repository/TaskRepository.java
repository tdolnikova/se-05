package com.dolnikova.tm.repository;

import com.dolnikova.tm.entity.Task;

import java.util.*;

public class TaskRepository implements Repository<Task> {

    private static LinkedHashMap<String, Task> tasks = new LinkedHashMap<>();

    @Override
    public List<Task> findAll() {
        List<Task> allTasks = new ArrayList<>();
        for (Map.Entry<String, Task> task : tasks.entrySet()) {
            allTasks.add(task.getValue());
        }
        return allTasks;
    }

    @Override
    public void removeAll() {
        tasks.clear();
    }

    @Override
    public void persist(Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public Task findOne(String id) {
        return tasks.get(id);
    }

    @Override
    public void merge(String id, Task task) {
        task.setId(id);
        tasks.put(id, task);
    }

    @Override
    public void remove(String id) {
        tasks.remove(id);
    }

    @Override
    public void remove(Task task) {
        tasks.remove(task.getId());
    }

    public LinkedHashMap<String, Task> getTasks() {
        return tasks;
    }

}
