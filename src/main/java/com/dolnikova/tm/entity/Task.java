package com.dolnikova.tm.entity;

import java.util.Date;
import java.util.UUID;

public class Task extends Entity{

    private String id;
    private String projectId;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;

    public Task() {}

    public Task(String projectId, String name) {
        this.name = name;
        id = UUID.randomUUID().toString();
        this.projectId = projectId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
