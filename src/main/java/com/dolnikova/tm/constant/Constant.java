package com.dolnikova.tm.constant;

public class Constant {

    /*
     * General messages
     * */

    public final static String WELCOME = "* * * Welcome to Task Manager * * *";
    public final static String APP_EXIT = "Выход из программы.";
    public final static String COMMAND_DOESNT_EXIST = "Такой команды не существует";
    public final static String ID = "ID";
    public final static String COLON = ": ";
    public final static String INCORRECT_NUMBER = "Некорректный ввод числа.";
    public final static String TRY_AGAIN = "Попробуйте еще раз.";

    /*
     * Help messages
     * */

    public final static String HELP_STRING = Constant.HELP + Constant.COLON + Constant.HELP_DESCRIPTION + "\n"
            + Constant.FIND_ALL_PROJECTS + Constant.COLON + Constant.FIND_ALL_PROJECTS + "\n"
            + Constant.FIND_PROJECT + Constant.COLON + Constant.FIND_PROJECT + "\n"
            + Constant.PERSIST_PROJECT + Constant.COLON + Constant.PERSIST_PROJECT + "\n"
            + Constant.MERGE_PROJECT + Constant.COLON + Constant.MERGE_PROJECT + "\n"
            + Constant.REMOVE_PROJECT + Constant.COLON + Constant.REMOVE_PROJECT + "\n"
            + Constant.REMOVE_ALL_PROJECTS + Constant.COLON + Constant.REMOVE_ALL_PROJECTS + "\n"
            + Constant.FIND_ALL_TASKS + Constant.COLON + Constant.FIND_ALL_TASKS + "\n"
            + Constant.FIND_TASK + Constant.COLON + Constant.FIND_TASK + "\n"
            + Constant.PERSIST_TASK + Constant.COLON + Constant.PERSIST_TASK + "\n"
            + Constant.MERGE_TASK + Constant.COLON + Constant.MERGE_TASK + "\n"
            + Constant.REMOVE_TASK + Constant.COLON + Constant.REMOVE_TASK + "\n"
            + Constant.REMOVE_ALL_TASKS + Constant.COLON + Constant.REMOVE_ALL_TASKS + "\n"
            + Constant.EXIT + Constant.COLON + Constant.EXIT;

    public final static String HELP = "help";
    public final static String HELP_DESCRIPTION = "show all commands";

    public final static String FIND_ALL_PROJECTS = "find all projects";
    public final static String FIND_ALL_PROJECTS_DESCRIPTION = "find all projects";
    public final static String FIND_PROJECT = "find project";
    public final static String FIND_PROJECT_DESCRIPTION = "find a project";
    public final static String PERSIST_PROJECT = "create project";
    public final static String PERSIST_PROJECT_DESCRIPTION = "create a project";
    public final static String MERGE_PROJECT = "update project";
    public final static String MERGE_PROJECT_DESCRIPTION = "update a project";
    public final static String REMOVE_PROJECT = "delete project";
    public final static String REMOVE_PROJECT_DESCRIPTION = "delete a project";
    public final static String REMOVE_ALL_PROJECTS = "delete all projects";
    public final static String REMOVE_ALL_PROJECTS_DESCRIPTION = "delete all projects";

    public final static String FIND_ALL_TASKS = "find all tasks";
    public final static String FIND_ALL_TASKS_DESCRIPTION = "find all tasks";
    public final static String FIND_TASK = "find task";
    public final static String FIND_TASK_DESCRIPTION = "find a task";
    public final static String PERSIST_TASK = "create task";
    public final static String PERSIST_TASK_DESCRIPTION = "create a task";
    public final static String MERGE_TASK = "update task";
    public final static String MERGE_TASK_DESCRIPTION = "update a task";
    public final static String REMOVE_TASK = "delete task";
    public final static String REMOVE_TASK_DESCRIPTION = "delete a task";
    public final static String REMOVE_ALL_TASKS = "delete all tasks";
    public final static String REMOVE_ALL_TASKS_DESCRIPTION = "delete all tasks";

    public final static String EXIT = "exit";
    public final static String EXIT_DESCRIPTION = "exit";

    /*
    * Project messages
    * */

    public final static String CHOOSE_PROJECT = "Выберите проект";
    public final static String PROJECT_NAME = "Найден проект: ";
    public final static String IN_PROJECT = "В проекте ";
    public final static String PROJECT = "Проект";
    public final static String CREATED_M = "создан.";
    public final static String NO_PROJECTS = "Проектов нет.";
    public final static String PROJECT_UPDATED = "Проект обновлен.";
    public final static String PROJECT_DELETED = "Проект удален.";
    public final static String INSERT_PROJECT_NAME = "Введите название проекта.";
    public final static String PROJECT_NAME_DOESNT_EXIST = "Проекта с таким названием не существует.";
    public final static String PROJECT_NAME_ALREADY_EXIST = "Проект с таким названием уже существует.";
    public final static String WHICH_PROJECT_DELETE = "Какой проект хотите удалить?";
    public final static String NO_TASKS_IN_PROJECT = "В выбранном проекте нет задач";

    /*
     * Task messages
     * */

    public final static String TASK = "Задача";
    public final static String NO_TASKS = "Нет задач.";
    public final static String TASK_ADDITION_COMPLETED = "Создание задач завершено.";
    public final static String CREATED_F = "создана.";
    public final static String INSERT_TASK = "Введите задачу: ";
    public final static String INSERT_NEW_TASK = "Введите новую задачу";
    public final static String INSERT_TASK_ID = "Введите id задачи.";
    public final static String INSERT_NEW_PROJECT_NAME = "Введите название нового проекта.";
    public final static String TASK_UPDATED = "Задача изменена.";
    public final static String TASK_DELETED = "Задача удалена.";
    public final static String OF_TASKS_WITH_POINT = "задач(и)";

}
