package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;

public class ProjectRemoveAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return Constant.REMOVE_ALL_PROJECTS;
    }

    @Override
    public String description() {
        return Constant.REMOVE_ALL_PROJECTS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        Bootstrap.projectService.removeAll();
    }
}
