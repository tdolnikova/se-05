package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;

import java.util.List;

public class ProjectFindAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return Constant.FIND_ALL_PROJECTS;
    }
    @Override
    public String description() {
        return Constant.FIND_ALL_PROJECTS_DESCRIPTION;
    }
    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        List<Project> allProjects = Bootstrap.projectService.findAll();
        if (allProjects.isEmpty()) System.out.println(Constant.NO_PROJECTS);
        else {
            for (Project project : allProjects) {
                System.out.println(project.getName());
            }
        }
    }
}
