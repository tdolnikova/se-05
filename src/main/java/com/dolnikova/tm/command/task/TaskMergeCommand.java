package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;

import java.util.ArrayList;

public class TaskMergeCommand extends AbstractCommand {
    @Override
    public String command() {
        return Constant.MERGE_TASK;
    }

    @Override
    public String description() {
        return Constant.MERGE_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        Project project = findProject();
        if (project == null) return;
        ArrayList<Task> projectTasks = Bootstrap.taskService.getTasksByProjectId(project.getId());
        int taskSize = projectTasks.size();
        if (taskSize == 0) {
            System.out.println(Constant.NO_TASKS_IN_PROJECT);
            return;
        }
        System.out.println(Constant.IN_PROJECT + project.getName() + " " + taskSize + " " + Constant.OF_TASKS_WITH_POINT + ":");
        for (Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(Constant.INSERT_TASK_ID);
        boolean taskUpdated = false;
        while (!taskUpdated) {
            String taskIdToUpdate = Bootstrap.scanner.nextLine();
            if (taskIdToUpdate.isEmpty()) break;
            System.out.println(Constant.INSERT_NEW_TASK);
            String newTaskText = Bootstrap.scanner.nextLine();
            if (newTaskText.isEmpty()) break;
            Task taskToAdd = new Task(project.getId(), newTaskText);
            Bootstrap.taskService.merge(taskIdToUpdate, taskToAdd);
            System.out.println(Constant.TASK_UPDATED);
            taskUpdated = true;
        }
    }

    public Project findProject() {
        if (Bootstrap.taskService.findAll().isEmpty()) {
            System.out.println(Constant.NO_TASKS);
            return null;
        }
        if (Bootstrap.projectService.findAll().isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return null;
        }
        System.out.println(Constant.CHOOSE_PROJECT);
        Project project = null;
        while (project == null) {
            String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = Bootstrap.projectService.findOne(projectName);
            if (project == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
        }
        return project;
    }

}
