package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Task;

import java.util.List;

public class TaskFindAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return Constant.FIND_ALL_TASKS;
    }

    @Override
    public String description() {
        return Constant.FIND_ALL_TASKS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        List<Task> allTasks = Bootstrap.taskService.findAll();
        if (allTasks.isEmpty()) System.out.println(Constant.NO_TASKS);
        else {
            for (Task task : allTasks) {
                System.out.println(task.getName());
            }
        }
    }
}
