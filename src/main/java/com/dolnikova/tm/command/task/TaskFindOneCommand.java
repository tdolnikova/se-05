package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;

import java.util.ArrayList;

public class TaskFindOneCommand extends AbstractCommand {
    @Override
    public String command() {
        return Constant.FIND_TASK;
    }

    @Override
    public String description() {
        return Constant.FIND_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        Project project = findProject();
        if (project == null) return;
        ArrayList<Task> projectTasks = Bootstrap.taskService.getTasksByProjectId(project.getId());
        System.out.println(Constant.IN_PROJECT + project.getName() + " " + projectTasks.size() + " " + Constant.OF_TASKS_WITH_POINT + ":");
        for (Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(Constant.INSERT_TASK_ID);
        Task task = null;
        while (task == null) {
            String taskId = Bootstrap.scanner.nextLine();
            if (taskId.isEmpty()) break;
            task = Bootstrap.taskService.findOne(taskId);
            if (task == null) System.out.println(Constant.INCORRECT_NUMBER);
            else System.out.println(Constant.ID + Constant.COLON + taskId + " " + task.getName());
        }

    }

    public Project findProject() {
        if (Bootstrap.taskService.findAll().isEmpty()) {
            System.out.println(Constant.NO_TASKS);
            return null;
        }
        if (Bootstrap.projectService.findAll().isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return null;
        }
        System.out.println(Constant.CHOOSE_PROJECT);
        Project project = null;
        while (project == null) {
            String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = Bootstrap.projectService.findOne(projectName);
            if (project == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
        }
        return project;
    }
}
