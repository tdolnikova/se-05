package com.dolnikova.tm.command.exit;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;

public class ExitCommand extends AbstractCommand {
    @Override
    public String command() {
        return Constant.EXIT;
    }

    @Override
    public String description() {
        return Constant.EXIT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }
}
