package com.dolnikova.tm.service;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectService implements Service<Project> {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project findOne(String projectName) {
        if (projectRepository.getProjects().isEmpty()) return null;
        return projectRepository.findOne(projectName);
    }

    @Override
    public void persist(Project project) {
        projectRepository.persist(project);
    }

    @Override
    public void merge(String newProjectName, Project projectToMerge) {
        projectRepository.merge(newProjectName, projectToMerge);
    }

    @Override
    public void remove(Project project) {
        projectRepository.remove(project.getId());
        ArrayList<Task> projectTasks = Bootstrap.taskService.getTasksByProjectId(project.getId());
        if (projectTasks.size() == 0) return;
        for (Task task : projectTasks) {
            Bootstrap.taskRepository.remove(task.getId());
        }
    }

    @Override
    public void removeAll() {
        projectRepository.removeAll();
        Bootstrap.taskService.removeAll();
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

}
