package com.dolnikova.tm.service;

import com.dolnikova.tm.entity.Entity;

import java.util.List;

public interface Service<E extends Entity> {
    List<E> findAll();
    E findOne(String name);
    void persist(E entity);
    void merge(String newName, E entityToMerge);
    void remove(E entity);
    void removeAll();
}
