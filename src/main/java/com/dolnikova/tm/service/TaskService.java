package com.dolnikova.tm.service;

import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TaskService implements Service<Task> {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findOne(String name) {
        return taskRepository.findOne(name);
    }

    @Override
    public void persist(Task newTask) {
        taskRepository.persist(newTask);
    }

    @Override
    public void merge(String taskIdToUpdate, Task taskToAdd) {
        taskRepository.merge(taskIdToUpdate, taskToAdd);
    }

    @Override
    public void remove(Task task) {
        if (task != null) taskRepository.remove(task);
    }

    @Override
    public void removeAll() {
        taskRepository.removeAll();
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public ArrayList<Task> getTasksByProjectId(String projectId) {
        ArrayList<Task> projectTasks = new ArrayList<>();
        for (Map.Entry<String, Task> entry : taskRepository.getTasks().entrySet()) {
            if (entry.getValue().getProjectId().equals(projectId)) projectTasks.add(entry.getValue());
        }
        return projectTasks;
    }

}
